<?php 
ob_start();
$con = mysqli_connect("localhost", "root", "", "virtual") or die("Error " . mysqli_error($con));

//onclick createuser
if(isset($_POST['submit'])){

//assigning variables
	$title = $_POST['title'];
	$level = ($_POST['level']);
	$description = ($_POST['description']);
	
	
	//confirming if all fields are filled
if(!empty($title) && !empty($level) && !empty($description) ){
	
	//checking if the password and confirm password corresponds
	
		//check the database if the values alredy exist
		$sql = "SELECT * FROM course WHERE title = '$title'";
		$query = mysqli_query($con,$sql);
		

				//inserting into database
		if(mysqli_num_rows($query)==0){
			$sql = "INSERT INTO course (title,level,description) 
					VALUES('$title','$level, description')";
					
					
			//if task is accomplished
			if(mysqli_query($con,$sql)){
				echo "User Account has been created successfully...";
				
				header("Location:welcome.php");
				//exit;
			}
			
		}else{
			echo "This course (" . $title . ") already exist. ";
		}
		

	
}else{ echo "All fields are required....";}
	
}

ob_end_flush();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Admin Page</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link  rel="stylesheet" type="text/css" href="style.css" >

    <!-- Bootstrap -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
 <div class="container-fluid display-table">
<div class="row display-table-row" >
<div class="col-md-3 display-table-cell" id="side-menu">
 <?php
 include ("side.php");
 ?>
 </div>
 
 
<div class="col-md-9 display-table-cell valign-top " >
 <div class="row">
 <?php
 include ("content.php");
 ?>
 </div>
 <div class="row affix-row">
		<div class="col-sm-9 col-md-10 affix-content">
			<div class="container">
				<div class="page-header">
					
					<button class="btn btn-success disabled text-uppercase"><span class="glyphicon glyphicon-folder-open"></span> <?php echo "ADD COURSE";?></button>
				</div>
				<p>
					
					<div class="row">
  						<div class="col-lg-5">
							<div class="progress pull-right">
								<div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
									<a href="../cancel.php" style="color:#fff;">CANCEL </a>
								</div>
							</div>
							<div class="panel panel-info">
								<div class="panel-body">
									<div class="progress">
  										<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
 	   										<span class="sr-only">STATUS</span>
  										</div>
									</div>
									<div class="btn-group" role="group" aria-label="...">
										<button class="btn btn-default disabled">STATUS</button>
										<button class="btn btn-primary disabled">ADD</button>
									</div>
									<form action="c.php" method="post">
										
										<span class="text-uppercase" style="font-weight:600!important;">COURSE</span>
										
										<div class="input-group">
  											<span class="input-group-addon"><span class="glyphicon glyphicon-pencil"></span></span>
											<input type="text" name="title" class="form-control" required>
										</div>
										
										<span class="text-uppercase" style="font-weight:600!important;">LEVELS</span>
										
										<div class="input-group">
  											<span class="input-group-addon"><span class="glyphicon glyphicon-pencil"></span></span>
											<input type="text" name="level" class="form-control" required>
										</div>
										
										<span class="text-uppercase" style="font-weight:600!important;">COURSE DESCRIPTION</span>
										
										<div class="input-group">
  											<span class="input-group-addon"><span class="glyphicon glyphicon-pencil"></span></span>
											<textarea  name="description" class="form-control" cols="30" rows="10" required></textarea>
										</div>
										<!--sub class="label label-warning pull-right"><?php printf("max Length: %d\n",$fieldinfo->max_length);?></sub-->
										
										<input class="btn btn-info" name="submit" type="submit" value="ADD ENTRIES">
										<input class="btn btn-warning" type="reset" value="CLEAR" > 
									</form>
								</div>
							</div>
						</div>
						
						
					</div><!--form div-->
				</p>
			</div>
		</div>
	</div>
 
 

 </div>
</div>
</div>
 
 
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>

	</body>
</html>