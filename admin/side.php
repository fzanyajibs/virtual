<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Admin Page</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link  rel="stylesheet" type="text/css" href="style.css" >

    <!-- Bootstrap -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

<h1>Navigation</h1>
<ul>
<li class="link active">
<a href="welcome.php">
<span class="glyphicon glyphicon-th" aria-hidden="true" ></span>
<span >Dashboard</span>
</a>
</li>

<li class="link">
<a href="#collapse-post" data-toggle="collapse" aria-controls="collapse-post">
<span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
<span>Human Resource</span>
</a>
<ul class="collapse collapseable" id="collapse-post">
<li><a href="viewadmin.php"> Manage Admins</a></li>
<li><a href="course.php"> Register Course</a></li>
</ul>
</li>

<li class="link">
<a href="#collapse-students" data-toggle="collapse" aria-controls="collapse-students">
<span class="glyphicon glyphicon-user" aria-hidden="true" ></span>
<span >Manage Students</span>
</a>
<ul class="collapse collapseable" id="collapse-students">
<li>
<a href="request.php"> View Requests</a>
</li>
<li><a href="viewstudent.php"> Student Database</a></li>
</ul>
</li>

<li class="link">
<a href="#collapse-teachers" data-toggle="collapse" aria-controls="collapse-teachers">
<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
<span>Teachers</span>
</a>
<ul class="collapse collapseable" id="collapse-teachers">
<li>
<a href="tregister.php"> Add Teacher</a>
<span class="label label-sucess pull-right">10</span>
</li>
<li><a href="viewteacher.php"> Teachers Database</a></li>
</ul>
</li>


<li class="link">
<a href="#collapse-calendar" data-toggle="collapse" aria-controls="collapse-calendar">
<span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
<span>Calender</span>
</a>
<ul class="collapse collapseable" id="collapse-calendar">
<li>
<a href="event.php"> View Events</a>
</li>
<li><a href="calendar.php"> Add Event</a></li>
</ul>
</li>

<li class="link">
<a href="mail.php">
<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
<span>Mail</span>
</a>
</li>

<li class="link">
<a href="exam.php">
<span class="glyphicon glyphicon-education" aria-hidden="true"></span>
<span>Exam</span>
</a>
</li>

<li class="link">
<a href="library.php">
<span class="glyphicon glyphicon-book" aria-hidden="true"></span>
<span>Library</span>
</a>
</li>

<li class="link">
<a href="blog.php">
<span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
<span>Blog</span>
</a>
</li>

<li class="link">
<a href="reports.php">
<span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>
<span>Reports</span>
</a>
</li>

<li class="link settings-btn">
<a href="settings.php">
<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
<span>Settings</span>
</a>
</li>

</ul>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>

	</body>
</html>