<?php

include('connection.php');

if (isset($_POST['submit']) && $_FILES['userfile']['size'] > 0) {
	
$allowedExts = array("jpg","jpeg","gif","png");

$location = "posts/";
$fileName = $_FILES['userfile']['name'];
$tmpName  = $_FILES['userfile']['tmp_name'];
$fileSize = $_FILES['userfile']['size'];
$fileType = $_FILES['userfile']['type'];

$temp = explode(".", $_FILES["userfile"]["name"]);
$extension = end($temp);

   
   if (in_array($extension, $allowedExts)) {

   $destination = $location.basename($fileName);
   move_uploaded_file($tmpName,$destination);


if(!get_magic_quotes_gpc())
{
    $fileName = addslashes($fileName);
}
 $content=mysqli_real_escape_string($con,$_POST['rte1']);
 $headline =  mysqli_real_escape_string($con,$_POST['headline']);	
 $date =  mysqli_real_escape_string($con,$_POST['dd']);
 
 $sql="INSERT INTO posts (content,headline,date,img,size,type)VALUES('$content','$headline','$date','$fileName','$fileSize','$fileType')";
  //$sql="INSERT INTO products (item,desc,price,color,img,size,type)VALUES('$item','$desc','$price','$color','$fileName','$fileSize','$fileType')";

 $result=mysqli_query($con,$sql);	
 
       echo"<center><p> Record saved! Redirecting...</p></center>";
       //echo"<META http-equiv='refresh' content='3;URL=./'>";     
	
   }
   else {
	   echo"<center><p>Only picture file ( with .jpg, .jpeg, .gif and .png extensions) are allowed </p></center>";
       echo"<META http-equiv='refresh' content='3;URL=./'>";                        
        }
}


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Admin Page</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link  rel="stylesheet" type="text/css" href="style.css" >
<script language="JavaScript" type="text/javascript" src="richtext_compressed.js"></script>
<script language="JavaScript" type="text/javascript" src="richtext.js"></script>
  
    <!-- Bootstrap -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
 <div class="container-fluid display-table">
<div class="row display-table-row" >
<div class="col-md-3 display-table-cell" id="side-menu">
 <?php
 include ("side.php");
 ?>
 </div>
 
 
<div class="col-md-9 display-table-cell valign-top " >
 <div class="row">
 <?php
 include ("content.php");
 ?>
 </div>
 <div class="row affix-row">
		<div class="col-sm-9 col-md-10 affix-content">
			<div class="container">
				<div class="page-header">
					
					<button class="btn btn-success disabled text-uppercase"><span class="glyphicon glyphicon-folder-open"></span> <?php echo "BLOG";?></button>
				</div>
				<p>
					
					<div class="row">
  						<div class="col-lg-5">
							<div class="progress pull-right">
								<div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
									<a href="welcome.php" style="color:#fff;">CANCEL </a>
								</div>
							</div>
							<div class="panel panel-info">
								<div class="panel-body">
									<div class="progress">
  										<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
 	   										<span class="sr-only">STATUS</span>
  										</div>
									</div>
									<div class="btn-group" role="group" aria-label="...">
										<button class="btn btn-default disabled">STATUS</button>
										<button class="btn btn-primary disabled">ADD</button>
									</div>
									<form  method="post" enctype="multipart/form-data" name="form1"  id="form1" onSubmit="submitForm()">
  <table width="423" height="244" border="0">
    
    
    <tr>
      <td>Desc:</td>
      <td>
           <script language="JavaScript" type="text/javascript">
<!--
function submitForm() {
  //make sure hidden and iframe values are in sync before submitting form
  //to sync only 1 rte, use updateRTE(rte)
  //to sync all rtes, use updateRTEs
  updateRTE('rte1');
  //updateRTEs();
  //alert("rte1 = " + document.form1.rte1.value);
  rte1 = document.form1.rte1.value;
  //change the following line to true to submit form
  return false;
}

//Usage: initRTE(imagesPath, includesPath, cssFile)
initRTE("images/", "", "");
//-->
</script>
<noscript><p><b>Javascript must be enabled to use this form.</b></p></noscript>

<script language="JavaScript" type="text/javascript">
<!--
//Usage: writeRichText(fieldname, html, width, height, buttons, readOnly)
writeRichText('rte1', '', 300, 200, true, false);
//-->
</script>
      </td>
      
    </tr></br>

   </tr></tr>

    <tr>
      <td>headlines:</td>
      <td><input type="text" name="headline" id="textfield2" /></td>
    </tr>
   </tr></tr>

	
	<tr>
      <td>Date:</td>
      <td><input type="date" name="dd"  class="box"required /></td>
    </tr>
      </tr></tr>

    <tr>
      <td>upload image</td>
      <td><input type="file" name="userfile" id="userfile"> 
      <input type="hidden" name="MAX_FILE_SIZE" value="2000000" /></td>
    </tr>
	   </tr></tr>

    <tr>
      <td><button class="btn btn-primary pull-right" name="submit" type="Submit">Post</button></td>&nbsp;&nbsp;
	        <td><a href ="posts.php" class="btn btn-primary">View Post</a></td>

    </tr>
  </table>
  <p>&nbsp;</p>
</form>
								</div>
							</div>
						</div>
						
						
					</div><!--form div-->
				</p>
			</div>
		</div>
	</div>
 
 

 </div>
</div>
</div>
 
 
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>

	</body>
</html>