<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Admin Page</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link  rel="stylesheet" type="text/css" href="style.css" >

    <!-- Bootstrap -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
 <div class="container-fluid display-table">
<div class="row display-table-row" >
<div class="col-md-3 display-table-cell" id="side-menu">
 <?php
 include ("side.php");
 ?>
 </div>
 
 
<div class="col-md-9 display-table-cell valign-top " >
 <div class="row">
 <?php
 include ("content.php");
 ?>
 </div>
 <div class="row affix-row">
		<div class="col-sm-9 col-md-10 affix-content">
			<div class="container">
				<div class="page-header">
					<br>
					<button class="btn btn-success disabled text-uppercase"><span class="glyphicon glyphicon-folder-open"></span> ADMIN LOG</button>
				</div>
				<p>
					
					<div class="progress pull-right">
						<div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="50" style="width: 100%">
							<a href="welcome.php" style="color:#fff;">CANCEL</a>
						</div>
					</div>
					<div class="panel panel-info">
						<div class="panel-body">
							<div class="progress">
  								<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
    									<span class="sr-only">STATUS</span>
  								</div>
							</div>
							<div class="btn-group" role="group" aria-label="...">
								<button class="btn btn-info " style="width:250px; ">STATUS</button>

							</div>



							<div class="btn-group" role="group" style="float:right;" aria-label="...">
								<a href="createadmin.php" class="btn btn-primary">ADD</a>
							</div>







<div class="table-responsive">
							<table  class="table table-striped colo-md-9">
	<tr style="background-color:orange; color:white;">						
<th>EMAIL</th>
<th>PASSWORD</th>
<th>ACTION</th>
<th></th>

</tr>
							<?php 
include ("connection.php");
$sql = "SELECT * FROM events ORDER BY id ";
$query = mysqli_query($con,$sql);

while($record = mysqli_fetch_array($query)){
$email=$record['email'];
		$password=$record['password'];
	echo "<tr>
			<td>		  <input type=\"text\" name=\"email\" class=\"student\" value=\"$email\"  style=\"   border:none;background:transparent;outline:none;height:40px;\"required />
</td>
			<td>		  <input type=\"text\" name=\"password\" class=\"student\" value=\"$password\"  style=\"   border:none;background:transparent;outline:none;width:100px;\"required />
			</td>
			<td><a class='btn btn-danger' href='admindelete.php?id={$record['id']}'>DELETE</a></td>
			<td><a class='btn btn-primary' href='adminedit.php?id={$record['id']}'>EDIT</a></td>

		</tr>";
}

?>
							
							</table>
</div>
<?php 
								$sql = "SELECT * FROM event"; 
								$rs_result = mysqli_query($con,$sql); //run the query
								$total_records = mysqli_num_rows($rs_result);  //count number of records
								$total_pages = ceil($total_records ); 
							?>
							
							<button class="btn btn-warning disabled" type="button" style="background:#ec971f;"><?php echo "table admin"; ?> has <span class="badge"><?php echo $total_records; ?></span> entries</button>
							<ul class="pagination pagination-sm pull-right">
								<?php
									echo "<li><a href='v.php?page=1'>".'|'."<span class='glyphicon glyphicon-chevron-left'></span></a></li> "; // Goto 1st page
									for ($i=1; $i<=$total_pages; $i++)
									{ 
            								echo "<li><a href='v.php?page=".$i."'>".$i."</a></li> "; 
									} 
									echo "<li><a href='v.php?page=$total_pages'><span class='glyphicon glyphicon-chevron-right'></span>".'|'."</a> </li>"; // Goto last page
								?>
							</ul>
</div>
</div>
</div>

</div>
	</div>


 </div>
</div>
</div>
 
 
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>

	</body>
</html>