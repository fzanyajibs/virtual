<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Admin Page</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link  rel="stylesheet" type="text/css" href="style.css" >

    <!-- Bootstrap -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
<header id="nav-header" class="clearfix">

<div class="col-md-5">
<input type="text" id="header-search-field" placeholder="Search for something">
</div>
<div class="col-md-7">
<ul class="pull-right" id="welcome">
<li>Welcome  Admin </li>
<li class="fixed-width">
<a>
<span class="glyphicon glyphicon-bell" aria-hidden="true" ></span>
<span class="label label-warning">3</span>
</a>
</li>
<li class="fixed-width">
<a>
<span class="glyphicon glyphicon-envelope" aria-hidden="true" ></span>
<span class="label label-message">3</span>
</a>
</li>

<li>
<a href="adminprogile.php" >
<span class="glyphicon glyphicon-edit " aria-hidden="true" ></span>
Edit Profile
</a>
</li>

<li>
<a href="#" class="logout">
<span class="glyphicon glyphicon-log-out " aria-hidden="true" ></span>
logout
</a>
</li>
</ul>

</div>

</header>

	</body>
</html>