<?php include_once('functions.php'); ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Admin Page</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link  rel="stylesheet" type="text/css" href="style.css" >
	<link type="text/css" rel="stylesheet" href="jscript/style.css"/>
<link type="text/css" rel="stylesheet" href="jscript/bootstrap/css/bootstrap.css"/>
<link type="text/css" rel="stylesheet" href="jscript/bootstrap/css/bootstrap.min.css"/>
<script src="jscript/jquery.min.js"></script>

    <!-- Bootstrap -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <div class="container-fluid display-table">
<div class="row display-table-row" >
<!-- side menu-->
<div class="col-md-2 display-table-cell" id="side-menu">
<h1>Navigation</h1>
<ul>
<li class="link active">
<a href="#">
<span class="glyphicon glyphicon-th" aria-hidden="true" ></span>
<span >Dashboard</span>
</a>
</li>

<li class="link">
<a href="#collapse-post" data-toggle="collapse" aria-controls="collapse-post">
<span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
<span>Human Resource</span>
</a>
<ul class="collapse collapseable" id="collapse-post">
<li><a href="createadmin.php"> Register Admins</a></li>
<li><a href="viewadmin.php"> View Admins</a></li>
<li><a href="course.php"> Register Course</a></li>
</ul>
</li>

<li class="link">
<a href="#collapse-students" data-toggle="collapse" aria-controls="collapse-students">
<span class="glyphicon glyphicon-user" aria-hidden="true" ></span>
<span >Manage Students</span>
</a>
<ul class="collapse collapseable" id="collapse-students">
<li>
<a href="request.php"> View Requests</a>
</li>
<li><a href="viewstudent.php"> View Students</a></li>
</ul>
</li>

<li class="link">
<a href="#collapse-teachers" data-toggle="collapse" aria-controls="collapse-teachers">
<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
<span>Teachers</span>
</a>
<ul class="collapse collapseable" id="collapse-teachers">
<li>
<a href="tregister.php"> Register Teacher</a>
<span class="label label-sucess pull-right">10</span>
</li>
<li><a href="viewteacher.php"> View Teachers</a></li>
</ul>
</li>


<li class="link">
<a href="calendar.php">
<span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
<span>Calender</span>
</a>
</li>

<li class="link">
<a href="mail.php">
<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
<span>Mail</span>
</a>
</li>

<li class="link">
<a href="exam.php">
<span class="glyphicon glyphicon-education" aria-hidden="true"></span>
<span>Exam</span>
</a>
</li>

<li class="link">
<a href="library.php">
<span class="glyphicon glyphicon-book" aria-hidden="true"></span>
<span>Library</span>
</a>
</li>

<li class="link">
<a href="blog.php">
<span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
<span>Blog</span>
</a>
</li>

<li class="link">
<a href="reports.php">
<span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>
<span>Reports</span>
</a>
</li>

<li class="link settings-btn">
<a href="settings.php">
<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
<span>Settings</span>
</a>
</li>

</ul>
</div>
<!-- main content-->
<div class="col-md-10 display-table-cell valign-top " >
<div class="row">
<header id="nav-header" class="clearfix">

<div class="col-md-5">
<input type="text" id="header-search-field" placeholder="Search for something">
</div>
<div class="col-md-7">
<ul class="pull-right" id="welcome">
<li>Welcome to Admin Page</li>
<li class="fixed-width">
<a>
<span class="glyphicon glyphicon-bell" aria-hidden="true" ></span>
<span class="label label-warning">3</span>
</a>
</li>
<li class="fixed-width">
<a>
<span class="glyphicon glyphicon-envelope" aria-hidden="true" ></span>
<span class="label label-message">3</span>
</a>
</li>

<li>
<a href="adminprogile.php" >
<span class="glyphicon glyphicon-edit " aria-hidden="true" ></span>
Edit Profile
</a>
</li>

<li>
<a href="#" class="logout">
<span class="glyphicon glyphicon-log-out " aria-hidden="true" ></span>
logout
</a>
</li>
</ul>

</div>

</header>
</div>

<div class="row">
<div id="calendar_div" class="container">
<h1 align="center">EVENT CALENDAR</h1>
	<?php echo getCalender(); ?>
</div>
</div>

<div class="row" >
<footer id="admin-footer" class="clearfix">
<div class="pull-left" ><b>Copyright</b>&copy; 2018</div>
<div class="pull-right">admin system</div>
</footer>
</div>
</div>

</div>
  </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>

	</body>
</html>