<!DOCTYPE html>
<html lang="en">
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title></title>
    <link  rel="stylesheet" type="text/css" href="styleme.css" >
    <link  rel="stylesheet" type="text/css" href="css/style.css" >

    <!-- Bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <div class="container-fluid">
  <div class="row">
  <?php
  include ("header.php");
  ?>
  </div>
  <div class="row lesson">
  <div class="col-md-6 phase1" >
  <p>COMPLETE SOLUTION FOR YOUR EDUCATION NEEDS</p>
  <h6>We offer courses and qualifications in a wide range of vocational and academic subjects at many levels.</h6>
  <a href="http://localhost/school/student/register.php"><button class="button1">ENROLL</button></a>
    <a href="courses.php"><button class="button2">View Courses</button></a>

  </div>
  
  <div class="col-md-6 video">
  
  <video width="550" height="440" controls>
  <source src="https://res.cloudinary.com/dommo7bjq/video/upload/v1528299407/tutorial.mp4" type="video/mp4">
</video>
  </div>
  </div>
  
  
  
  <div class=" row proflogo">
  <p>LEARN AT YOUR OWN PACE </p>
      <img src="images/f1.png"  >
      <img src="images/banks.jpg" >
      <img src="images/ican2.jpg" >
      <img src="images/law.png" >
      <img src="images/management.jpg" >
      <img src="images/managers.jpg" >
      <img src="images/marketers.jpg" >

  
  </div>
  
  <div class=" row container">
		<a href="contact.php" class="info-request">
			<span class="holder">
				<span class="title">Request information</span>
				<span class="text">Do you have some questions? Fill the form and get an answer!</span>
			</span>
			<span class="arrow"></span>
		</a>
	</div>
  
  <div class="row">
  <?php
  include ("footer.php");
  ?>
  </div>
  </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>