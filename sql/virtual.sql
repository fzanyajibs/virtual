-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 23, 2018 at 05:54 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `virtual`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`) VALUES
(5, 'mimi@gmailcom', 'mimi'),
(6, 'steve@gmail.com', 'steve');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `levels` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `title`, `levels`, `description`) VALUES
(2, 'Java', '2', 'hello'),
(3, 'ikc', '2', 'jhgfdfghjkl'),
(4, 'cpn', '2', 'lkjhgf');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=Block',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `title`, `date`, `created`, `modified`, `status`) VALUES
(1, 'wedding', '2018-03-01', '2018-03-22 20:47:02', '0000-00-00 00:00:00', 1),
(2, 'birthday 2pm', '2018-03-01', '2018-03-22 21:33:08', '0000-00-00 00:00:00', 1),
(3, 'christmas', '2018-12-25', '2018-03-22 21:41:49', '2018-03-22 21:41:49', 1),
(4, '', '2018-03-22', '2018-03-22 21:47:32', '2018-03-22 21:47:32', 1),
(5, '', '0000-00-00', '2018-03-22 21:54:29', '2018-03-22 21:54:29', 1),
(6, 'birthday', '2018-04-17', '2018-04-12 11:06:49', '2018-04-12 11:06:49', 1);

-- --------------------------------------------------------

--
-- Table structure for table `gravator`
--

CREATE TABLE IF NOT EXISTS `gravator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `gravator`
--


-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) NOT NULL,
  `headline` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `img` mediumblob NOT NULL,
  `size` varchar(255) NOT NULL,
  `type` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `content`, `headline`, `date`, `img`, `size`, `type`) VALUES
(1, '', ';lkjhgc', '2018-03-22', 0x333932303938325f313437313130333334333634355f313830783138302e6a7067, '9491', 'image/jpeg'),
(2, 'kjhiuygf', 'kojihuygtf', '2018-03-14', 0x333231303938345f696d672d32303136303532362d7761303030325f313830783137332e6a7067, '6743', 'image/jpeg'),
(3, 'mnjbhgc', 'lkjhgf', '2018-04-19', 0x333932303938325f313437313130333334333634355f313830783138302e6a7067, '9491', 'image/jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `password` varchar(25) NOT NULL,
  `mobile` int(25) NOT NULL,
  `dob` varchar(20) NOT NULL,
  `address` varchar(20) NOT NULL,
  `course` varchar(20) NOT NULL,
  `approved` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `email_2` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `username`, `gender`, `email`, `password`, `mobile`, `dob`, `address`, `course`, `approved`) VALUES
(15, 'samuel', 'Male', 'samju@gmail.com', '1234', 70652134, '2019-03-14', 'abuja', 'ICAN', 1),
(17, 'okoh', 'Female', 'jojo@gmail.com', '', 0, '2018-03-14', 'lagos', 'ICAN', 1),
(19, 'uchennapat', 'Male', 'uche@gmail.com', 'uchenna2001', 2147483647, '2018-03-22', 'imo', 'ICAN', 0);

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE IF NOT EXISTS `teachers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `address` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `gender` varchar(25) NOT NULL,
  `qualification` varchar(100) NOT NULL,
  `nationality` varchar(25) NOT NULL,
  `course` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `fname`, `phone`, `address`, `email`, `password`, `gender`, `qualification`, `nationality`, `course`) VALUES
(1, 'ESIAGU', '07063512587', 'lagos', 'esiaguchimezieleticia@roc', '1234', 'Female', 'BSC. computer science', 'nigerian', 'ICAN'),
(2, 'Ejimogu', '081765344', 'Lagos', 'esiagunwakaego@yahoo.com', 'tochi', 'Male', 'BSC. computer Enginering', 'American', 'ICAN'),
(3, 'ESIAGU', '07063512587', 'lagos', 'uleticia@gmail.com', 'nwakaego1993', 'Male', 'BSC. computer science', 'nigerian', 'ICAN'),
(4, 'ESIAGU', '07063512587', 'lagos', 'mimi@gmail.com', 'nwakaego1993', 'Male', 'BSC. computer science', 'nigerian', 'ICAN');

-- --------------------------------------------------------

--
-- Table structure for table `text`
--

CREATE TABLE IF NOT EXISTS `text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(500) NOT NULL,
  `image` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `category` varchar(20) NOT NULL,
  `description` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `text`
--

INSERT INTO `text` (`id`, `file`, `image`, `title`, `category`, `description`) VALUES
(16, '../books/help.docx', '../books/3.jpg', 'java2', 'ICAN', '1234'),
(15, '../books/02-corejava.pdf', '../books/1-an-asymmetrical-side-sweep.jpg', 'java', 'ICAN', 'kjh');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
