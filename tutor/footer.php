<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Admin Page</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link  rel="stylesheet" type="text/css" href="style.css" >

    <!-- Bootstrap -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
  </head>
  <body>
  <div class="container-fluid">
  <div class="row">
  <footer id="admin-footer" class="clearfix">
<div class="pull-left" ><b>Copyright</b>&copy; 2018</div>
<div class="pull-right">admin system</div>
</footer>
  </div>
  
  </div>
  
  
  
  
  
  
  
  <script src="js/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>
	</body>
</html>